package main

import (
	"fmt"
	"os"
)

func main() {
	var number int
	fmt.Print("Введите трёхзначное число: ")
	if _, ok := fmt.Scan(&number); ok != nil {
		fmt.Println("Некорректный формат ввода.", "\n(", ok, ")")
		os.Exit(1)
	}

	if number >= 1000 || number < 100 {
		fmt.Println(" Не верное значение. число должно находиться в диапазоне от 100 до 999 ")
	} else {
		fmt.Println(" Количество сотен в числе ", number, " = ", number/100)
		fmt.Println(" Колличество десятков в числе  ", number, " = ", (number%100)/10)
		fmt.Println(" Колличество чисел в 1-ом разпяде числа   ", number, " = ", (number%100)%10)
	}
}
