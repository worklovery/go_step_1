package main

import (
	"fmt"
	"math"
	"os"
)

func main() {
	var s, r float64
	fmt.Print("Введите площадь круга: ")
	if _, ok := fmt.Scan(&s); ok != nil {
		fmt.Println("Некорректный формат ввода.", "\n(", ok, ")")
		os.Exit(1)
	}

	if s <= 0 {
		fmt.Println(" Не верное значение площади ")
	} else {
		r = math.Sqrt(s / math.Pi)
		fmt.Println(" Длина окружности площатью ", s, " = ", 2*math.Pi*r)
		fmt.Println(" Диаметр окружности площатью ", s, " = ", 2*r)
	}
}
