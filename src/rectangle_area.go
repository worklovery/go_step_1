package main

import (
	"fmt"
	"os"
)

func main() {
	var a, b float32
	fmt.Print("Введите значения сторон прямоугольника: ")
	if _, ok := fmt.Scan(&a, &b); ok != nil {
		fmt.Println("Некорректный формат ввода.", "\n(", ok, ")")
		os.Exit(1)
	}

	if a <= 0 || b <= 0 {
		fmt.Println(" Площадь прямоугольника не может быть вычислена! ")
	} else {
		fmt.Println(" Площадь прямоугольника со сторонами ", a, " и ", b, "=", a*b)
	}
}
